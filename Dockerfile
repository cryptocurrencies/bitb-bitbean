FROM ubuntu:16.04
LABEL Description="This image is used to build Bitbean from github master"

RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list

RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libssl-dev git-core libboost-all-dev libdb4.8-dev libdb4.8++-dev libminiupnpc-dev pkg-config sudo && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN git clone https://github.com/TeamBitBean/bitbean-core.git /opt/bitbean && \
    cd /opt/bitbean/src && \
    make -f makefile.unix  && \
    cp /opt/bitbean/src/BitBeand /usr/local/bin/ && \
    rm -rf /opt/bitbean

RUN groupadd -r bitbean && useradd -r -m -g bitbean bitbean

ENV GLOBALCOIN_DATA /data

RUN mkdir $GLOBALCOIN_DATA

COPY BitBean.conf $GLOBALCOIN_DATA/BitBean.conf

RUN chown bitbean:bitbean $GLOBALCOIN_DATA && \
    ln -s $GLOBALCOIN_DATA /home/bitbean/.BitBean

USER bitbean
VOLUME /data

EXPOSE 22460 22461

CMD ["/usr/local/bin/BitBeand", "-conf=/data/BitBean.conf", "-server", "-txindex", "-printtoconsole"]